﻿namespace Api_Test.DataObjects
{
    public class GetCommitsResponse
    {
        public BitbucketCommit[] Values { get; set;  }
    }
}
