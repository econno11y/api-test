﻿using Api_Test.DataObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Api_Test
{
    public static class PossibleActions
    {
        public static async Task<bool> RunApiTest()
        {
            bool success = false;
            HttpClient client = new HttpClient();
            string requestUri = "https://api.bitbucket.org/2.0/repositories/calanceus/api-test/commits?fields=values.message,next";
            HttpResponseMessage response = await client.GetAsync(requestUri);

            GetCommitsResponse commits = null;
            if (response.IsSuccessStatusCode)
            {
                string responseAsString = await response.Content?.ReadAsStringAsync();
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                };

                try
                {
                    commits = JsonConvert.DeserializeObject<GetCommitsResponse>(responseAsString, settings);
                } 
                catch (JsonSerializationException)
                {
                    throw;
                }
            }
            else
            {
                throw new HttpRequestException($"Problem making get request to bitbuck api test repository. Status: {response?.ReasonPhrase}");
            }

            string outputFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Commits.txt");

            try
            {
                File.WriteAllLines(outputFile, commits?.Values?.Select(c => c.Message)?.Reverse());
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException("Found no commits to write to txt file.");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            success = true;
            return success;
        }
    }
}
