﻿using System;
using System.Net;
using System.Net.Http;

namespace Api_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            bool success = false;
            Console.WriteLine("Would you like to overwrite commits.txt with all the commit messages checked into https://bitbucket.org/calanceus/api-test/src/master/?");
            ConsoleKeyInfo answer = Console.ReadKey();

            if (answer.Key == ConsoleKey.Y)
            {
                try
                {
                    success = PossibleActions.RunApiTest().Result;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"\n{ex.Message}");
                    return;
                }
            } 
            else
            {
                Console.WriteLine("\nI'm sorry to hear that.  That's all I can do.  Goodbye.");
            }

            if (success)
            {
                Console.WriteLine("\nSuccess!");
            }
        }
    }
}
